import operator


def getTruth(inp, relate, cut):
    op = str(relate).upper()
    operators = {
        '>': operator.gt,
        '<': operator.lt,
        'ASC': operator.gt,
        'DESC': operator.lt
    }

    return operators[op](inp, cut)


class Node:
    def __init__(self, key, right=None, left=None):
        self.left = left
        self.right = right
        self.key = key

    def addNode(self, node, sorting):
        if getTruth(self.key, sorting, node.key):
            if self.left:
                self.left.addNode(node, sorting)
            else:
                self.left = node
        else:
            if self.right:
                self.right.addNode(node, sorting)
            else:
                self.right = node
