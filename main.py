from node import Node
from binaryTree import BinaryTree as Tree


def dummyCallback(msg):
    print(str(msg))


if __name__ == '__main__':
    root = Node(1)
    tree = Tree(root, sorting='desc')

    tree.addNode(Node(2))
    tree.addNode(Node(3))
    tree.addNode(Node(44))
    tree.addNode(Node(424))
    tree.addNode(Node(532))
    tree.addNode(Node(555))
    tree.addNode(Node(52))
    tree.addNode(Node(53))
    tree.addNode(Node(5323))
    tree.addNode(Node(523))
    tree.addNode(Node(21))
    tree.addNode(Node(19))
    tree.addNode(Node(3))
    tree.addNode(Node(5))

    print('\n>>>in order desc')
    tree.inOrder(root)
