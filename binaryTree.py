class BinaryTree:
    def __init__(self, root, name='Binary Tree', sorting='asc'):
        self.root = root
        self.name = name
        self.sorting = sorting
        print(self.sorting)

    def preOrder(self, node, cb=print):
        cb(node.key)
        if node.left: self.preOrder(node.left)
        if node.right: self.preOrder(node.right)

    def inOrder(self, node, cb=print):
        if node.left: self.inOrder(node.left)
        cb(node.key)
        if node.right: self.inOrder(node.right)

    def postOrder(self, node, cb=print):
        if node.left: self.postOrder(node.left)
        if node.right: self.postOrder(node.right)
        cb(node.key)

    def addNode(self, node):
        if self.root:
            self.root.addNode(node, self.sorting)
        else:
            self.root = node
